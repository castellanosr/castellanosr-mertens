public class Weeee {

    public static void weeee() {
        System.out.println("Weeee!");
        weeee();
    }
    /**The program prints "Weeee!" because of the println command, but
     * then continues to print the statement because of the second
     * "weeee" after the ";" this causes an error because the computer
     * cannot handle the amount of power required.
    **/

    public static void main(String[] args) {
        weeee();
    }

}
