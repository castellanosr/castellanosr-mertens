public class Hooray {

    public static void hooray() {
        while(true) {
            System.out.println("Hooray!");
        }
    } /** the while loop will always be true so "Hooray!" infinitely
    * printed in the terminal window.
    **/

    public static void main(String[] args) {
        hooray();
    }

}
